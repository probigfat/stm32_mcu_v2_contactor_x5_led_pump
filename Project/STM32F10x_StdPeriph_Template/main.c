#include"stm32f10x_rcc.h"
#include"stm32f10x_gpio.h"
#include"misc.h"
#include"stm32f10x_usart.h"
#include"stdio.h"
#define FlashData_ ((uint16_t *)(0x08002000))
#define FlashData (*FlashData_)
#define ON 1
#define OFF 0

#define MotorR_F	(Out2=1)
#define MotorR_B 	(Out2=0)
#define MotorL_F 	(Out4=1)
#define MotorL_B 	(Out4=0)
#define Motor_ON 	(Out1=1)
#define Motor_OFF (Out1=0)
#define Pump_ON 	(Out5=1)
#define Pump_OFF 	(Out5=0)
#define Pump_GET 	(Out5)
#define LED_ON 		(Out6=1)
#define LED_OFF 	(Out6=0)
#define LED_GET 	(Out6)
#define CLOCK_RST	(my_clock[5]=0)
#define CLOCK_GET	(my_clock[5])
#define RELAY_TIM1	1000  //继电器动作时间1 40ms用作换向继电器的动作时间
#define RELAY_TIM2	10   	//继电器动作时间2 40ms用作总通继电器的动作时间



enum STATUS_{S=0,F=1,B=3,R=5,L=7,mF=11,mB=13,mR=17,mL=19};//0停止态 1前进态 3后退态 5右转态 7左转态 11/13/17/19 中间态
typedef enum STATUS_ MODE;


int my_init(void);
int delay(unsigned int);
int my_RCC_init(void);
int my_Flash_init(void);
int my_Usart_init(void);
int my_GPIO_init(void);
int my_Nvic_init(void);
int uart1_printf(const char *buf,unsigned n);
int my_SysTick_init(void);


int OutPut_refresh(void);//刷新驱动模块的输出
int Input_refresh(void);//刷新无线模块的输入
MODE Get_Intend_Status(void);//判断返回目的状态
int Status_S(void);//停止状态处理
int Status_F(void);//前进状态处理
int Status_B(void);//后退状态处理
int Status_R(void);//右转状态处理
int Status_L(void);//左转状态处理
int Status_mF(void);//过渡前进状态处理
int Status_mB(void);//过渡后退状态处理
int Status_mR(void);//过渡右转状态处理
int Status_mL(void);//过渡左转状态处理
int Pump_Control(void);//吸污泵处理
int LEDs_Control(void);//探照灯处理


unsigned int irq_flag=1;
volatile uint16_t usart_char='A';
unsigned int my_timer=0;
unsigned int my_clock[10];
volatile uint32_t SysTick_ms=0;//来自systick生成的1ms向上计数器

//6路输入
unsigned char key_value=0;
unsigned char key_value_old=0;
unsigned char key1;
unsigned char key2;
unsigned char key3;
unsigned char key4;
unsigned char key5;
unsigned char key6;


//8路输出
unsigned char Out1=0;//总电机开关
unsigned char Out2=0;//右电机
unsigned char Out3=0;
unsigned char Out4=0;//左电机
unsigned char Out5=0;//吸污泵
unsigned char Out6=0;//LED探照灯
unsigned char Out7=0;
unsigned char Out8=0;


MODE Mode_Current=S;//当前状态
MODE Mode_Old=S;		//上一个有效状态
MODE Mode_Intent=S;	//目的状态

int my_init(void)
{
	my_RCC_init();
	my_SysTick_init();
	my_Nvic_init();
	my_GPIO_init();	
	my_Usart_init();
	return 0;
}

int delay(unsigned int i)
{
//universal delay function,it is simple and not accurate !!!
	unsigned int m;
	unsigned int n;
	for(m=i;m>0;m--)
		for(n=i;n>0;n--);
	return 0;
}


void EXTI15_10_IRQHandler()
{
//here is the exit15 irq server function
	uart1_printf("here is irq handler \r\n",100);

	EXTI_ClearITPendingBit(EXTI_Line15);
	delay(100);
	if(!(0x01&GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_15)))
			irq_flag++;
}

void USART1_IRQHandler()
{

//here we deal with uart1 irq
	
	if(USART_GetFlagStatus(USART1, USART_FLAG_RXNE))
	{
			USART_ClearITPendingBit(USART1, USART_IT_RXNE);
			usart_char++;
			if(usart_char >126)
				usart_char = 33;
	};
}


int uart1_printf(const char *buf,unsigned max)
{
	//usually,we use this function to debug,here we use the source of usart1,we  should not transfer more than 1024 bytes once;
	//it will not config the uart and gpio,so we should do it by yourselve.
	unsigned int i;	
	if(max>1024)
		max=1024;
	
	for(i=0;i<max && buf[i] != '\0';i++)
	{
		USART_SendData(USART1,buf[i]);
		while(!USART_GetFlagStatus(USART1, USART_FLAG_TXE));		
	}		
	return i;
}


int my_GPIO_init(void)
{
	
	GPIO_InitTypeDef GPIO_Init_struct_temp;
	EXTI_InitTypeDef EXTI_InitStruct_temp;
	NVIC_InitTypeDef NVIC_InitStruct_temp;
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_DeInit (GPIOB);		
	//herer we light up the Led on core board to tell others that we have fired up the system successfully,at least once;
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_11;
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOB,GPIO_Pin_11);	
	//This gpiob.5 is used to as OutPut
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_5;
	GPIO_ResetBits(GPIOB,GPIO_Pin_5);//close the OutPut
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOB,GPIO_Pin_5);//close the OutPut
	//This gpiob.6 is used to as OutPut
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_6;
	GPIO_ResetBits(GPIOB,GPIO_Pin_6);//close the OutPut
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOB,GPIO_Pin_6);//close the OutPut
	//This gpiob.7 is used to as OutPut
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_7;
	GPIO_ResetBits(GPIOB,GPIO_Pin_7);//close the OutPut
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOB,GPIO_Pin_7);//close the OutPut
	//This gpiob.8 is used to as OutPut
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_8;
	GPIO_ResetBits(GPIOB,GPIO_Pin_8);//close the OutPut
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOB,GPIO_Pin_8);//close the OutPut
	
	//This gpiob.12 is used to as InputMode
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_12;
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);
	//This gpiob.14 is used to as InputMode
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_14;
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);
	//This gpiob.13 is used to as InputMode
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_13;
	GPIO_Init(GPIOB, &GPIO_Init_struct_temp);
	
	
	
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);//we need to use gpio's interrupt,so enable the AFIO clock;	
//init the gpio port c 15 as input with up pull
	GPIO_DeInit (GPIOC);
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_15;
	GPIO_Init(GPIOC, &GPIO_Init_struct_temp);
//open the irq of pc.15
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource15);
//here we config the exit controler ,choose line15 which is connected to gpio.15s.
	EXTI_DeInit();
	EXTI_InitStruct_temp.EXTI_Line=EXTI_Line15;
	EXTI_InitStruct_temp.EXTI_LineCmd=ENABLE;
	EXTI_InitStruct_temp.EXTI_Mode=EXTI_Mode_Interrupt;//not event mode,we choose interrupt mode
	EXTI_InitStruct_temp.EXTI_Trigger=EXTI_Trigger_Falling;//falling edge trigger
	EXTI_Init(&EXTI_InitStruct_temp);
//here we config the NVIC registers.
	NVIC_InitStruct_temp.NVIC_IRQChannel=EXTI15_10_IRQn;
	NVIC_InitStruct_temp.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct_temp.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStruct_temp.NVIC_IRQChannelSubPriority=2;
	NVIC_Init(&NVIC_InitStruct_temp);

	
	//here open the gpioa and usart1,as they two use the same output pins	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	
	//init the gpio porta.9 as push and pull output by phri device,uart's Tx pin
	GPIO_DeInit(GPIOA);
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_9;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);	
	//init the gpio porta.10 as input floating mode by phri device,uart's Rx pin
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IN_FLOATING;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_10;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
	
	//This gpioa.1 is used to as OutPut
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_1;
	GPIO_ResetBits(GPIOA,GPIO_Pin_1);//close the OutPut
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOA,GPIO_Pin_1);//close the OutPut
	//This gpioa.2 is used to as OutPut
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_2;
	GPIO_ResetBits(GPIOA,GPIO_Pin_2);//close the OutPut
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOA,GPIO_Pin_2);//close the OutPut
	//This gpioa.3 is used to as OutPut
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_3;
	GPIO_ResetBits(GPIOA,GPIO_Pin_3);//close the OutPut
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOA,GPIO_Pin_3);//close the OutPut
	//This gpioa.4 is used to as OutPut
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_4;
	GPIO_ResetBits(GPIOA,GPIO_Pin_4);//close the OutPut
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);	
	GPIO_ResetBits(GPIOA,GPIO_Pin_4);//close the OutPut
	
	//This gpioa.15 is used to as InputMode
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_15;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
	//This gpiob.12 is used to as InputMode
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_12;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
	//This gpiob.8 is used to as InputMode
	GPIO_Init_struct_temp.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init_struct_temp.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_struct_temp.GPIO_Pin=GPIO_Pin_8;
	GPIO_Init(GPIOA, &GPIO_Init_struct_temp);
	

	
	return 0;
}

int my_Nvic_init(void)
{
	//here we do some universe operation about nvic controllor
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//group the interrupts here we set it as 2 pre and 2 sub
	return 0;
}


void TIM2_IRQHandler()
{	
	TIM_ClearITPendingBit(TIM2,TIM_IT_Update);	
	my_timer++;
//	uart1_printf("\r\n here is TIM2 IRQ Hnadler \r\n",100);	
}


int my_RCC_init(void)
{
	RCC_DeInit();

	RCC_HSEConfig(RCC_HSE_ON);
	RCC_WaitForHSEStartUp();
	RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9); //8X9=72MHz,
	RCC_PLLCmd(ENABLE);
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);//choose pll as clock source
	RCC_HCLKConfig(RCC_SYSCLK_Div1);//set AHB as 72/2Mhz
	RCC_PCLK1Config(RCC_HCLK_Div2);//set AHB1 Low speed as 36MHz,RCC_HCLK_Div2
	RCC_PCLK2Config(RCC_HCLK_Div1);//set AHB2 High speed as 72MHz
	return 0;
}


int my_Usart_init(void)
{
	USART_InitTypeDef USART_InitStruct_temp;
	USART_ClockInitTypeDef USART_ClockInitStruct_temp;
	NVIC_InitTypeDef NVIC_InitStruct_temp;
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);// open the clock of usart1
	
//then we start to init the usart1
	USART_DeInit(USART1);
  USART_StructInit(&USART_InitStruct_temp);
	USART_InitStruct_temp.USART_BaudRate=115200;
	USART_Init(USART1, &USART_InitStruct_temp);
  USART_ClockStructInit(&USART_ClockInitStruct_temp);
	USART_ClockInit(USART1, &USART_ClockInitStruct_temp);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE );
//USART_ITConfig(USART1, USART_IT_TXE, ENABLE ); there is no need to use send ok irq,because we can decide this time.
  USART_Cmd(USART1, ENABLE);
	
	NVIC_InitStruct_temp.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct_temp.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStruct_temp.NVIC_IRQChannelPreemptionPriority=3 ;//
	NVIC_InitStruct_temp.NVIC_IRQChannelSubPriority = 3;        //           //
	NVIC_Init(&NVIC_InitStruct_temp); 
	
	
	return 0;
}




int my_SysTick_init(void)
{
	//init the SysTick to generate a pulse of 1ms
	
	while(SysTick_Config(9000)==1);//等待systick设置成功
	
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);//无效果，奇怪
	return 0;
	
}

int SysTick_Handler(void)
{
	unsigned int i=0;
	
	SysTick_ms++;
	
	for(i=0;i<10;i++)
	{
		if(my_clock[i]<65000)
			my_clock[i]+=1;
	}
	return 0;
	
	/*
	clock1：开关防抖、废弃暂不用
	clock5：状态计时
	clock4：LED灯切换按钮计时，防抖
	clock6：吸污泵切换按钮计时，防抖
	*/
}


int Input_refresh(void)//刷新输入
{
	key1=GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12);//前进
	key2=GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_13);//后退	
	key3=GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14);//右转
	key4=GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8);//左转
	key5=GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_12);//吸污泵开关
	key6=GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_15);//探照灯开关
	
	key_value=0;
	if(key1==Bit_RESET)
		key_value+=1;
	if(key2==Bit_RESET)
		key_value+=2;
	if(key3==Bit_RESET)
		key_value+=4;
	if(key4==Bit_RESET)
		key_value+=8;
	if(key5==Bit_RESET)
		key_value+=16;
	if(key6==Bit_RESET)
		key_value+=32;	
		
	return 0;
}

int OutPut_refresh(void)//刷新输出
{
	if(Out1==0)
		GPIO_ResetBits(GPIOA,GPIO_Pin_1);
	else
		GPIO_SetBits(GPIOA,GPIO_Pin_1);
	
	if(Out2==0)
		GPIO_ResetBits(GPIOA,GPIO_Pin_2);
	else
		GPIO_SetBits(GPIOA,GPIO_Pin_2);
	
	if(Out3==0)
		GPIO_ResetBits(GPIOA,GPIO_Pin_3);
	else
		GPIO_SetBits(GPIOA,GPIO_Pin_3);
	
	if(Out4==0)
		GPIO_ResetBits(GPIOA,GPIO_Pin_4);
	else
		GPIO_SetBits(GPIOA,GPIO_Pin_4);
	
	if(Out5==0)
		GPIO_ResetBits(GPIOB,GPIO_Pin_5);
	else
		GPIO_SetBits(GPIOB,GPIO_Pin_5);
	
	if(Out6==0)
		GPIO_ResetBits(GPIOB,GPIO_Pin_6);
	else
		GPIO_SetBits(GPIOB,GPIO_Pin_6);
	
	if(Out7==0)
		GPIO_ResetBits(GPIOB,GPIO_Pin_7);
	else
		GPIO_SetBits(GPIOB,GPIO_Pin_7);
	
	if(Out8==0)
		GPIO_ResetBits(GPIOB,GPIO_Pin_8);
	else
		GPIO_SetBits(GPIOB,GPIO_Pin_8);	
	
	return 0;
}


MODE Get_Intend_Status(void)
{
	MODE mode_temp=S;
	

			if(key_value==1)//前进模式判断
			{
				mode_temp=F;//前进			
			}
			if(key_value==2)//前进模式判断
			{
				mode_temp=B;//后退			
			}
			if(key_value==4)//前进模式判断
			{
				mode_temp=R;//右转		
			}
			if(key_value==8)//前进模式判断
			{
				mode_temp=L;//左转			
			}
			if(key_value!=1 && key_value!=2 && key_value!=4 && key_value!=8)
			{
				mode_temp=S;//停止
			}	
			
	if(my_clock[1]>10 )//响应延迟，防止抖动&& key_value_old!=key_value
	{			my_clock[1]=0;//响应计时复位			
			key_value_old=key_value;
	}
	return mode_temp;
}


int Status_S(void)//停止状态处理
{
	if(Mode_Current==S)
	{
		if(Mode_Intent==S)
		{
		;		
		}
		if(Mode_Intent==F)
		{
			MotorR_F;
			MotorL_F;
			Mode_Current=mF;//切换继电器，进入中间前进状态
			CLOCK_RST;
		}
		if(Mode_Intent==B)
		{
			MotorR_B;
			MotorL_B;
			Mode_Current=mB;//切换继电器，进入中间后退状态
			CLOCK_RST;
		}	
		if(Mode_Intent==R)
		{
			MotorR_F;
			MotorL_B;
			Mode_Current=mR;//切换继电器，进入中间右转状态
			CLOCK_RST;
		}
		if(Mode_Intent==L)
		{
			MotorR_B;
			MotorL_F;
			Mode_Current=mL;//切换继电器，进入中间左转状态
			CLOCK_RST;
		}

		Mode_Old=S;//刷新上个状态值.
	}
	return 0;
}

/****************************前进切换       start************************************************/
int Status_mF(void)//过渡前进状态处理
{
	if(Mode_Current==mF)
	{
		
		if(Mode_Intent==F)//
		{
			if(Mode_Old==F)
			{
				Mode_Current=F;
				Mode_Old=mF;
				CLOCK_RST;
			}
			else
			{
				if(CLOCK_GET>RELAY_TIM1)
				{
					Mode_Current=F;
					Mode_Old=mF;
					CLOCK_RST;
				}	
			}	
		}
		else
		{
			if(Mode_Old==F)
			{
				if(CLOCK_GET>RELAY_TIM1)
				{
					Mode_Current=S;
					Mode_Old=mF;
					CLOCK_RST;
				}	
			}
			else
			{
					Mode_Current=S;
					Mode_Old=mF;
					CLOCK_RST;
			}		
		}			
	}
	return 0;
}






int Status_F(void)//前进状态处理
{
	if(Mode_Current==F)
	{
		Motor_ON;
		if(Mode_Intent!=F)//
		{
			Motor_OFF;
			Mode_Current=mF;
			Mode_Old=F;
			CLOCK_RST;
		}		
	}
	return 0;
}

/****************************前进切换        end************************************************/







/****************************后退切换       start************************************************/
int Status_mB(void)//过渡后退状态处理
{
	if(Mode_Current==mB)
	{
		
		if(Mode_Intent==B)//
		{
			if(Mode_Old==B)
			{
				Mode_Current=B;
				Mode_Old=mB;
				CLOCK_RST;
			}
			else
			{
				if(CLOCK_GET>RELAY_TIM1)
				{
					Mode_Current=B;
					Mode_Old=mB;
					CLOCK_RST;
				}	
			}	
		}
		else
		{
			if(Mode_Old==B)
			{
				if(CLOCK_GET>RELAY_TIM1)
				{
					Mode_Current=S;
					Mode_Old=mB;
					CLOCK_RST;
				}	
			}
			else
			{
					Mode_Current=S;
					Mode_Old=mB;
					CLOCK_RST;
			}		
		}			
	}
	return 0;
}




int Status_B(void)//后退状态处理
{
	if(Mode_Current==B)
	{
		Motor_ON;
		if(Mode_Intent!=B)//
		{
			Motor_OFF;
			Mode_Current=mB;
			Mode_Old=B;
			CLOCK_RST;
		}		
	}
	return 0;
}

/****************************后退切换        end************************************************/





/****************************右转切换       start************************************************/
int Status_mR(void)//过渡右转状态处理
{
	if(Mode_Current==mR)
	{
		
		if(Mode_Intent==R)//
		{
			if(Mode_Old==R)
			{
				Mode_Current=R;
				Mode_Old=mR;
				CLOCK_RST;
			}
			else
			{
				if(CLOCK_GET>RELAY_TIM1)
				{
					Mode_Current=R;
					Mode_Old=mR;
					CLOCK_RST;
				}	
			}	
		}
		else
		{
			if(Mode_Old==R)
			{
				if(CLOCK_GET>RELAY_TIM1)
				{
					Mode_Current=S;
					Mode_Old=mR;
					CLOCK_RST;
				}	
			}
			else
			{
					Mode_Current=S;
					Mode_Old=mR;
					CLOCK_RST;
			}		
		}			
	}
	return 0;
}




int Status_R(void)//右转状态处理
{
	if(Mode_Current==R)
	{
		Motor_ON;
		if(Mode_Intent!=R)//
		{
			Motor_OFF;
			Mode_Current=mR;
			Mode_Old=R;
			CLOCK_RST;
		}		
	}
	return 0;
}

/****************************右转切换        end************************************************/





/****************************左转切换       start************************************************/
int Status_mL(void)//过渡左转状态处理
{
	if(Mode_Current==mL)
	{
		
		if(Mode_Intent==L)//
		{
			if(Mode_Old==L)
			{
				Mode_Current=L;
				Mode_Old=mL;
				CLOCK_RST;
			}
			else
			{
				if(CLOCK_GET>RELAY_TIM1)
				{
					Mode_Current=L;
					Mode_Old=mL;
					CLOCK_RST;
				}	
			}	
		}
		else
		{
			if(Mode_Old==L)
			{
				if(CLOCK_GET>RELAY_TIM1)
				{
					Mode_Current=S;
					Mode_Old=mL;
					CLOCK_RST;
				}	
			}
			else
			{
					Mode_Current=S;
					Mode_Old=mL;
					CLOCK_RST;
			}		
		}			
	}
	
	return 0;
}




int Status_L(void)//左转状态处理
{
	if(Mode_Current==L)
	{
		Motor_ON;
		if(Mode_Intent!=L)//
		{
			Motor_OFF;
			Mode_Current=mL;
			Mode_Old=L;
			CLOCK_RST;
		}		
	}
	
	return 0;
}

/****************************左转切换        end************************************************/




/****************************吸污泵处理       end************************************************/
int Pump_Control(void)//吸污泵处理
{
	if(key_value==16 && my_clock[6]>3000 )
	{
		if(Pump_GET ==ON)
			Pump_OFF;
		else
			Pump_ON;	
		my_clock[6]=0;
	}
	return 0;
	
}
/****************************吸污泵处理       end************************************************/



/****************************探照灯处理      start************************************************/
	int LEDs_Control(void)//探照灯处理
{
	if(key_value==32 && my_clock[4]>500 )
	{
		if(LED_GET ==ON)
			LED_OFF;
		else
			LED_ON;
		my_clock[4]=0;
	}
	return 0;	
}
/****************************探照灯处理       end************************************************/




int main(void)
{	
	char buffer[250];//需要修改startup.s内的栈size，突破默认容量限制
	my_init();		
  while (1)
  {	
		if( my_clock[2]>1000)
		{
			GPIO_ResetBits(GPIOB,GPIO_Pin_11);	//led on board	
		}
		else 
		{
			GPIO_SetBits(GPIOB,GPIO_Pin_11);
		}
		if( my_clock[2]>1000*2)
			 my_clock[2]=0;


		OutPut_refresh();//刷新输出
		Input_refresh();//刷新输入			
		Mode_Intent=Get_Intend_Status();//目的状态获取				
		Status_S();//停止状态处理
		Status_F();//前进状态处理
		Status_B();//后退状态处理
		Status_R();//右转状态处理
		Status_L();//左转状态处理
		Status_mF();//过渡前进状态处理
		Status_mB();//过渡后退状态处理
		Status_mR();//过渡右转状态处理
		Status_mL();//过渡左转状态处理
		Pump_Control();//吸污泵处理		
	}
}
